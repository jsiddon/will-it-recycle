package com.example.justin.testing;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UPCLookup extends AppCompatActivity {

    private static final String DEBUG_TAG = "HttpExample";
    private static final String EXTRA_MESSAGE = "com.example.justin.testing.MESSAGE";
    private TextView urlText;
    private TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upclookup);
        urlText = (TextView) findViewById(R.id.api_base_url);
        textView = (TextView) findViewById(R.id.response_text);
    }

    // My second method, attached to 'submit' upc code.
    public void sendUpc(View view) {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        String stringUrl = urlText.getText().toString();
        stringUrl = stringUrl + "xml/7b0ef3fc28082d0c1a3a8d9cdce71730/4029764001807";       // Lookup Club Mate.

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Log.v(DEBUG_TAG, "Downloading page.");
            // Fetch data because network is connected.
            new DownloadWebPage().execute(stringUrl);

        } else {
            // Display error and return.
            textView.setText("No network connection");
        }

//        Intent intent = new Intent(this, DisplayMessageActivity.class);
//
//        EditText editText = (EditText) findViewById(R.id.upc_code);
//
//        String message = editText.toString();
//        intent.putExtra(EXTRA_MESSAGE, message);
//
//        startActivity(intent);
    }

    // Download webpage.
    public class DownloadWebPage extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params come from the execute call: params[0] is url.
            try {
                Log.v(DEBUG_TAG, "Connecting...");
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
        // onPostExecture displays the results of AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            textView.setText(result);
        }
    }

    /**
     * Convert stream to string.
     */
    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    /**
     * Actually go and get info. Given a URL, establish an HTTP connection and retrieve
     * the web page content as a InputStream, which this method then returns as a stream.
     */
    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;

        // Display first 500 chars of returned page content.
        int len = 500;

        try {
            URL url = new URL(myurl);           // Create url object
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();              // Create http connection.
            conn.setReadTimeout(10000);     // Number of millis
            conn.setConnectTimeout(15000);  // Number of millis
            conn.setRequestMethod("GET");
            conn.setDoInput(true);

            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(DEBUG_TAG, "The response is: " + response);
            is = conn.getInputStream();

            // Convert the input stream to a string.
            String contentAsString = readIt(is, len);
            return contentAsString;
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    /**
     * Parse xml
     */
    public List parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List entries = new ArrayList();

        parser.require(XmlPullParser.START_TAG, null, "output");
        while(parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            Log.v(name, "test");
            entries.add(name);
        }
        return entries;

    }

}
