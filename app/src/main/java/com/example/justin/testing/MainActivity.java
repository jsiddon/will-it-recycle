package com.example.justin.testing;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.example.justin.testing.MESSAGE";
    private static final String DEBUG_TAG = "Debugging: ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // My first method.
    public void sendMessage(View view) {
        // Do something!
        Intent intent = new Intent(this, DisplayMessageActivity.class);

        // Get the edit text element, where the user put in text.
        EditText editText = (EditText) findViewById(R.id.edit_message);

        // Assign text to a local 'message' variable, putExtra() adds text value to intent.
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);            // Key name in first param, value in second.

        startActivity(intent);
    }

    // Calls the sendUpc method inside, UPCLookup.
    public void sendUpc(View view) {
        Log.v(DEBUG_TAG, "Downloading page.");                      // This is the last debug statement thats printing to console. 
        Intent intent = new Intent(this, UPCLookup.class);          // Intent takes the class the activity is coming from and passes it to the 2nd param.
        startActivity(intent);
    }


}
